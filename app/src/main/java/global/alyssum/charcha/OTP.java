package global.alyssum.charcha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class OTP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Pinview pinview = findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                if(pinview.getValue().equals("2019")){

                    Intent intent = new Intent(OTP.this, MainActivity.class);
                    OTP.this.startActivity(intent);

                } else{
                    Toast.makeText(OTP.this, "Incorrect otp", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
