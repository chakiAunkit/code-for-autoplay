package global.alyssum.charcha;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class login extends AppCompatActivity {


    RelativeLayout loginDetailss, buttonLayoutt;
    Handler handler = new Handler();

    Runnable runnable = new Runnable() {
            @Override
            public void run() {

                loginDetailss.setVisibility(View.VISIBLE);
                buttonLayoutt.setVisibility(View.VISIBLE);
            }
        };

    public void yay(View view) {

        Intent intent = new Intent(login.this, OTP.class);
        login.this.startActivity(intent);

        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        loginDetailss = findViewById(R.id.loginDetails);
        buttonLayoutt = findViewById(R.id.buttonLayout);

        handler.postDelayed(runnable, 3000);
    }
}
